package com.demo.gallery

import android.app.Application
import com.demo.gallery.di.component.ApplicationComponent
import com.demo.gallery.di.component.DaggerApplicationComponent
import com.demo.gallery.di.module.ApplicationModule

class GalleryApplication : Application() {

    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        injectDependencies()
    }

    private fun injectDependencies() {
        applicationComponent = DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
        applicationComponent.inject(this)
    }

    fun setComponent(applicationComponent: ApplicationComponent) {
        this.applicationComponent = applicationComponent
    }
}
