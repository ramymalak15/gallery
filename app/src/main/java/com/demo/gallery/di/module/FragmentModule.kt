package com.demo.gallery.di.module

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.demo.gallery.data.PhotoRepository
import com.demo.gallery.ui.main.HomePhotoSharedViewModel
import com.demo.gallery.ui.main.fragments.home.PhotoRecyclerViewAdapter
import com.demo.gallery.ui.main.fragments.photo.PhotoPagerAdapter
import com.demo.gallery.utils.factory.ViewModelProviderFactory
import com.demo.gallery.utils.rx.SchedulerProvider
import com.google.android.flexbox.*
import dagger.Module
import dagger.Provides
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.subjects.PublishSubject

@Module
class FragmentModule(private val fragment: Fragment) {

    @Provides
    fun providePhotoRecyclerViewAdapter(): PhotoRecyclerViewAdapter =
        PhotoRecyclerViewAdapter(ArrayList())

    @Provides
    fun provideFlexboxLayoutManager() = FlexboxLayoutManager(fragment.context).apply {
        flexWrap = FlexWrap.WRAP
        flexDirection = FlexDirection.ROW
        alignItems = AlignItems.STRETCH
        justifyContent = JustifyContent.SPACE_AROUND
    }

    @Provides
    fun providePhotoPagerAdapter(): PhotoPagerAdapter = PhotoPagerAdapter(fragment, ArrayList())

    @Provides
    fun provideHomePhotoSharedViewModel(
        compositeDisposable: CompositeDisposable,
        schedulerProvider: SchedulerProvider,
        photoRepository: PhotoRepository
    ): HomePhotoSharedViewModel = ViewModelProvider(
        fragment.requireActivity(), ViewModelProviderFactory(HomePhotoSharedViewModel::class) {
            HomePhotoSharedViewModel(
                compositeDisposable,
                schedulerProvider,
                photoRepository,
                PublishSubject.create(),
                ArrayList()
            )
        }).get(HomePhotoSharedViewModel::class.java)
}