package com.demo.gallery.di.component

import com.demo.gallery.di.ActivityScope
import com.demo.gallery.di.module.ActivityModule
import com.demo.gallery.ui.main.MainActivity
import dagger.Component

@ActivityScope
@Component(dependencies = [ApplicationComponent::class], modules = [ActivityModule::class])
interface ActivityComponent {
    fun inject(mainActivity: MainActivity)
}