package com.demo.gallery.di.module

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.demo.gallery.data.PhotoRepository
import com.demo.gallery.ui.main.HomePhotoSharedViewModel
import com.demo.gallery.ui.main.MainViewModel
import com.demo.gallery.utils.factory.ViewModelProviderFactory
import com.demo.gallery.utils.network.NetworkHelper
import com.demo.gallery.utils.network.NetworkWatcher
import com.demo.gallery.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.subjects.PublishSubject

@Module
class ActivityModule(private val activity: AppCompatActivity) {

    @Provides
    fun provideNetworkWatcher():NetworkWatcher = NetworkWatcher(activity)

    @Provides
    fun provideMainViewModel(networkWatcher: NetworkWatcher, networkHelper: NetworkHelper):MainViewModel  = ViewModelProvider(
        activity, ViewModelProviderFactory(MainViewModel::class) {
            MainViewModel(networkWatcher,networkHelper)
        }).get(MainViewModel::class.java)

    @Provides
    fun provideHomePhotoSharedViewModel(
        compositeDisposable: CompositeDisposable,
        schedulerProvider: SchedulerProvider,
        photoRepository: PhotoRepository
    ): HomePhotoSharedViewModel = ViewModelProvider(
        activity, ViewModelProviderFactory(HomePhotoSharedViewModel::class) {
            HomePhotoSharedViewModel(compositeDisposable, schedulerProvider, photoRepository, PublishSubject.create(), ArrayList())
        }).get(HomePhotoSharedViewModel::class.java)

}
