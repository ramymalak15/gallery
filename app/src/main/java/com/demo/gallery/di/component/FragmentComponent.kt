package com.demo.gallery.di.component

import com.demo.gallery.di.FragmentScope
import com.demo.gallery.di.module.FragmentModule
import com.demo.gallery.ui.main.fragments.photo.details.DetailsFragment
import com.demo.gallery.ui.main.fragments.home.HomeFragment
import com.demo.gallery.ui.main.fragments.photo.PhotoFragment
import dagger.Component

@FragmentScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [FragmentModule::class]
)
interface FragmentComponent {
    fun inject(fragment: PhotoFragment)
    fun inject(fragment: HomeFragment)
    fun inject(fragment: DetailsFragment)

}
