package com.demo.gallery.di.component

import android.app.Application
import android.content.Context
import com.demo.gallery.GalleryApplication
import com.demo.gallery.data.remote.NetworkService
import com.demo.gallery.di.module.ApplicationModule
import com.demo.gallery.utils.network.NetworkHelper
import com.demo.gallery.utils.rx.SchedulerProvider
import dagger.Component
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun inject(app: GalleryApplication)

    fun getApplication(): Application

    fun getContext(): Context

    fun getCompositeDisposable(): CompositeDisposable

    fun getNetworkService(): NetworkService

    fun getSchedulerProvider(): SchedulerProvider

    fun getNetworkHelper(): NetworkHelper

}