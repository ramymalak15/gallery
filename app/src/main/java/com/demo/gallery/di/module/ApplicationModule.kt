package com.demo.gallery.di.module

import android.app.Application
import android.content.Context
import com.demo.gallery.BuildConfig
import com.demo.gallery.GalleryApplication
import com.demo.gallery.data.remote.NetworkService
import com.demo.gallery.data.remote.Networking
import com.demo.gallery.utils.network.NetworkHelper
import com.demo.gallery.utils.rx.SchedulerProvider
import com.demo.gallery.utils.rx.SchedulerProviderImpl
import dagger.Module
import dagger.Provides
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: GalleryApplication) {

    @Provides
    @Singleton
    fun provideApplication(): Application = application

    @Provides
    @Singleton
    fun provideContext(): Context = application

    @Provides
    @Singleton
    fun provideNetworkService(): NetworkService = Networking.create(
        BuildConfig.BASE_URL,
        application.cacheDir,
        10L * 1024 * 1024,
        NetworkHelper(application)
    )

    @Provides
    fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    @Singleton
    fun provideSchedulerProvider(): SchedulerProvider = SchedulerProviderImpl()

    @Provides
    @Singleton
    fun provideNetworkHelper(): NetworkHelper = NetworkHelper(application)




}