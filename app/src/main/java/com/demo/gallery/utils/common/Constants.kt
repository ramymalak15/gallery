package com.demo.gallery.utils.common

object Constants {
    const val PHOTO_ITEM:String = "PHOTO_ITEM"
    const val CURRENT_POSITION:String = "CURRENT_POSITION"
}