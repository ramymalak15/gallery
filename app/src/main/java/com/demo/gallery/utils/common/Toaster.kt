package com.demo.gallery.utils.common

import android.content.Context
import android.widget.Toast

object Toaster {
    fun show(context: Context, message: String){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun show(context: Context, resource: Int){
        show(context, context.getString(resource))
    }
}