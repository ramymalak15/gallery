package com.demo.gallery.ui.main.fragments.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.demo.gallery.GalleryApplication
import com.demo.gallery.databinding.FragmentHomeBinding
import com.demo.gallery.di.component.DaggerFragmentComponent
import com.demo.gallery.di.module.FragmentModule
import com.demo.gallery.ui.main.HomePhotoSharedViewModel
import com.demo.gallery.utils.common.Toaster
import com.google.android.flexbox.FlexboxLayoutManager
import javax.inject.Inject
import javax.inject.Provider

class HomeFragment : Fragment() {
    companion object{
        const val TAG = "HomeFragment"
    }

    @Inject
    lateinit var homeViewModel: HomePhotoSharedViewModel

    @Inject
    lateinit var photoAdapter: PhotoRecyclerViewAdapter

    @Inject
    lateinit var flexLayoutManager: Provider<FlexboxLayoutManager>

    lateinit var binding: FragmentHomeBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentHomeBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencies()
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
        binding.photosRv.apply {
            adapter = photoAdapter
            layoutManager = flexLayoutManager.get()
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    layoutManager?.run {
                        if (this is FlexboxLayoutManager
                            && itemCount > 0
                            && itemCount == findLastVisibleItemPosition() + 1
                        ) homeViewModel.onLoadMore()
                    }
                }
            })
        }
        homeViewModel.initLoad()
    }


    private fun setupObservers(){
        homeViewModel.currentPosition.observe(viewLifecycleOwner, {
            it?.run {
                if((binding.photosRv.layoutManager as FlexboxLayoutManager).findFirstCompletelyVisibleItemPosition() != this){
                    binding.photosRv.scrollToPosition(this)
                }
            }
        })
        homeViewModel.errorMessage.observe(viewLifecycleOwner, {
            it?.run {
                Toaster.show(requireContext(), this)
            }
        })
        homeViewModel.errorMessageResource.observe(viewLifecycleOwner, {
            it?.run {
                Toaster.show(requireContext(), this)
            }

        })
        homeViewModel.isLoading.observe(viewLifecycleOwner, {
            binding.progress.visibility = if (it == true) View.VISIBLE else View.GONE
        })

        homeViewModel.photos.observe(viewLifecycleOwner, {
            it?.run {
                Log.d(TAG, "append list of size ${this.size}")
                photoAdapter.updateList(this)
            }
        })
    }

    private fun injectDependencies(){
        DaggerFragmentComponent
            .builder()
            .applicationComponent((requireContext().applicationContext as GalleryApplication).applicationComponent)
            .fragmentModule(FragmentModule(this))
            .build()
            .inject(this)
    }
}