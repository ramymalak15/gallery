package com.demo.gallery.ui.main.fragments.photo.details

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.palette.graphics.Palette
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.demo.gallery.GalleryApplication
import com.demo.gallery.R
import com.demo.gallery.data.remote.response.PhotoItem
import com.demo.gallery.databinding.FragmentDetailsBinding
import com.demo.gallery.di.component.DaggerFragmentComponent
import com.demo.gallery.di.module.FragmentModule
import com.demo.gallery.utils.common.Constants

class DetailsFragment : Fragment() {

    lateinit var binding: FragmentDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailsBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencies()
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getParcelable<PhotoItem>(Constants.PHOTO_ITEM)?.run {
            if (this.id?.toInt()!! < 0) {
                binding.image.setImageResource(R.drawable.ad_placeholder)
                BitmapFactory.decodeResource(resources,
                    R.drawable.ad_placeholder)?.run {
                    setBGColorDominant(this)
                }
                binding.progress.visibility = View.GONE
            } else {
                binding.progress.visibility = View.VISIBLE
                Glide
                    .with(requireContext())
                    .load(download_url)
                    .apply(RequestOptions.placeholderOf(R.drawable.ic_default))
                    .apply(RequestOptions.overrideOf(width, height))
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .fitCenter()
                    .addListener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            binding.progress.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            resource?.run {
                                setBGColorDominant(this.toBitmap())
                            }

                            binding.progress.visibility = View.GONE
                            return false
                        }

                    })
                    .into(binding.image)
            }
        }
    }

    private fun setBGColorDominant(bitmap: Bitmap) =
        Palette.Builder(bitmap).generate {
            it?.let { palette ->
                context?.run {
                    val dominantColor = palette.getDominantColor(
                        ContextCompat.getColor(
                            this,
                            R.color.black
                        )
                    )
                    binding.root.setBackgroundColor(dominantColor)
                }

            }
        }

    private fun injectDependencies() {
        DaggerFragmentComponent
            .builder()
            .applicationComponent((requireContext().applicationContext as GalleryApplication).applicationComponent)
            .fragmentModule(FragmentModule(this))
            .build()
            .inject(this)
    }
}