package com.demo.gallery.ui.main.fragments.home

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.appcompat.widget.AppCompatImageView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.demo.gallery.R
import com.demo.gallery.data.remote.response.PhotoItem
import com.demo.gallery.ui.main.fragments.PhotoDiffUtil
import com.demo.gallery.utils.common.Constants
import com.demo.gallery.utils.common.Toaster
import com.google.android.material.textview.MaterialTextView


class PhotoRecyclerViewAdapter(private val photos: ArrayList<PhotoItem>) :
    RecyclerView.Adapter<PhotoRecyclerViewAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val author: MaterialTextView = view.findViewById(R.id.author)
        val image: AppCompatImageView = view.findViewById(R.id.image)
        val progress: ProgressBar = view.findViewById(R.id.progress)

    }


    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_photo, viewGroup, false)

        return ViewHolder(view)
    }


    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val context = viewHolder.itemView.context
        viewHolder.author.text = photos[position].author
        if (photos[position].id?.toInt()!! < 0) {
            viewHolder.image.setImageResource(R.drawable.ad_placeholder)
            viewHolder.progress.visibility = View.GONE
        } else {
            viewHolder.progress.visibility = View.VISIBLE
            Glide
                .with(context)
                .load(photos[position].download_url)
                .apply(RequestOptions.placeholderOf(R.drawable.ic_default))
                .apply(RequestOptions.overrideOf(150, 100))
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .addListener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        viewHolder.progress.visibility = View.GONE
                        Toaster.show(context, R.string.image_loading_failed)
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        viewHolder.progress.visibility = View.GONE
                        return false
                    }

                })
                .into(viewHolder.image)
        }


        viewHolder.itemView.setOnClickListener {
            viewHolder.itemView.findNavController().navigate(
                R.id.action_from_home_to_detail_fragment,
                Bundle().apply {
                    putInt(Constants.CURRENT_POSITION, position)
                }
            )
        }

    }

    override fun getItemCount() = photos.size

    fun updateList(list: List<PhotoItem>) {
        val diffCallback = PhotoDiffUtil(photos, list)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        photos.clear()
        photos.addAll(list)
        diffResult.dispatchUpdatesTo(this)
    }

}
