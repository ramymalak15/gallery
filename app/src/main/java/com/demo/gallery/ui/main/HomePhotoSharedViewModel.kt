package com.demo.gallery.ui.main

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.demo.gallery.R
import com.demo.gallery.data.PhotoRepository
import com.demo.gallery.data.remote.response.PhotoItem
import com.demo.gallery.utils.rx.SchedulerProvider
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.subjects.PublishSubject

class HomePhotoSharedViewModel(
    private val compositeDisposable: CompositeDisposable,
    private val schedulerProvider: SchedulerProvider,
    private val photoRepository: PhotoRepository,
    private val paginator: PublishSubject<Int>,
    private val allPhotos: ArrayList<PhotoItem>
) : ViewModel() {

    companion object {
        const val TAG = "HPSharedViewModel"
    }

    val isLoading = MutableLiveData<Boolean>()
    val photos = MutableLiveData<List<PhotoItem>>()
    val errorMessage = MutableLiveData<String>()
    val errorMessageResource = MutableLiveData<Int>()
    private var currentPage = 1

    init {
        compositeDisposable.add(
            paginator
                .distinctUntilChanged()
                .doOnNext {
                    isLoading.postValue(true)
                }
                .concatMapSingle { page ->
                    return@concatMapSingle photoRepository
                        .fetchPhotos(page)
                        .subscribeOn(schedulerProvider.io())
                        .doOnError { e ->
                            isLoading.postValue(false)
                            Log.d(TAG, "error: ${e.localizedMessage}")
                            errorMessageResource.postValue(R.string.loading_result_failed)
                        }
                }
                .subscribe(
                    {
                        isLoading.postValue(false)
                        allPhotos.addAll(it)
                        allPhotos.add(
                            PhotoItem(
                                "ad placeholder",
                                "",
                                0,
                                (-1 * currentPage).toString(),
                                "",
                                0
                            )
                        )
                        photos.postValue(allPhotos)
                        Log.d(
                            TAG,
                            "$currentPage loaded starts with ${it?.first()?.author}"
                        )
                        currentPage += 1
                    },
                    { e ->
                        isLoading.postValue(false)
                        errorMessageResource.postValue(R.string.loading_result_failed)
                        Log.d(TAG, "error: ${e.localizedMessage}")
                    }
                )
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    private fun load(page: Int = currentPage) {
        paginator.onNext(page)
        Log.d(TAG, "get page $page")
    }

    fun onLoadMore() {
        load()
    }

    fun initLoad() {
        if (allPhotos.isNullOrEmpty())
            load(1)
    }

    val currentPosition = MutableLiveData<Int>()
    fun setCurrentPosition(position: Int) {
        currentPosition.value = position
    }
}
