package com.demo.gallery.ui.main.fragments.photo

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.demo.gallery.GalleryApplication
import com.demo.gallery.databinding.FragmentPhotoBinding
import com.demo.gallery.di.component.DaggerFragmentComponent
import com.demo.gallery.di.module.FragmentModule
import com.demo.gallery.ui.main.HomePhotoSharedViewModel
import com.demo.gallery.utils.common.Constants
import com.demo.gallery.utils.common.Toaster
import javax.inject.Inject

class PhotoFragment : Fragment() {
    companion object {
        const val TAG = "PhotoFragment"
    }

    @Inject
    lateinit var photoViewModel: HomePhotoSharedViewModel

    @Inject
    lateinit var photoPagerAdapter: PhotoPagerAdapter

    private lateinit var binding: FragmentPhotoBinding

    private val pageChangeCallback = object : ViewPager2.OnPageChangeCallback() {
        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {
            super.onPageScrolled(position, positionOffset, positionOffsetPixels)
            if (position == photoPagerAdapter.itemCount - 1) {
                photoViewModel.onLoadMore()
            }
        }

        override fun onPageSelected(position: Int) {
            super.onPageSelected(position)
            photoViewModel.setCurrentPosition(position)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPhotoBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencies()
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
        binding.photosVp.adapter = photoPagerAdapter
        photoViewModel.initLoad()
        arguments?.getInt(Constants.CURRENT_POSITION, -1)?.run {
            if(this != -1){
                photoViewModel.setCurrentPosition(this)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        binding.photosVp.registerOnPageChangeCallback(pageChangeCallback)
    }

    override fun onPause() {
        binding.photosVp.unregisterOnPageChangeCallback(pageChangeCallback)
        super.onPause()
    }

    private fun setupObservers() {
        photoViewModel.currentPosition.observe(viewLifecycleOwner, {
            it?.run {
                if(binding.photosVp.currentItem != this){
                    binding.photosVp.setCurrentItem(this, false)
                }
            }
        })
        photoViewModel.errorMessage.observe(viewLifecycleOwner, {
            it?.run {
                Toaster.show(requireContext(), this)
            }
        })
        photoViewModel.errorMessageResource.observe(viewLifecycleOwner, {
            it?.run {
                Toaster.show(requireContext(), this)
            }

        })
        photoViewModel.isLoading.observe(viewLifecycleOwner, {
            binding.progress.visibility = if (it == true) View.VISIBLE else View.GONE
        })

        photoViewModel.photos.observe(viewLifecycleOwner, {
            it?.run {
                Log.d(TAG, "append list of size ${this.size}")
                photoPagerAdapter.updateList(this)
                photoViewModel.currentPosition.value?.run {
                    binding.photosVp.setCurrentItem(this, false)
                }
            }
        })
    }

    private fun injectDependencies() {
        DaggerFragmentComponent
            .builder()
            .applicationComponent((requireContext().applicationContext as GalleryApplication).applicationComponent)
            .fragmentModule(FragmentModule(this))
            .build()
            .inject(this)
    }
}