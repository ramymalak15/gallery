package com.demo.gallery.ui.main


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.demo.gallery.utils.network.NetworkHelper
import com.demo.gallery.utils.network.NetworkWatcher

class MainViewModel(private val networkWatcher: NetworkWatcher, private val networkHelper: NetworkHelper) : ViewModel() {

    init {
        networkWatcher.start()
    }

    val isConnected:LiveData<Boolean> = networkWatcher.isNetworkConnected.apply {
        postValue(networkHelper.isNetworkConnected())
    }



    override fun onCleared() {
        networkWatcher.stop()
        super.onCleared()
    }


}
