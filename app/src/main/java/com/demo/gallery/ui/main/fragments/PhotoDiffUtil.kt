package com.demo.gallery.ui.main.fragments

import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil
import com.demo.gallery.data.remote.response.PhotoItem


class PhotoDiffUtil(private val oldList: List<PhotoItem>, private val newList: List<PhotoItem>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id === newList.get(newItemPosition).id
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        val (author1, download_url1, _) = oldList[oldPosition]
        val (author2, download_url2, _) = newList[newPosition]

        return author1 == author2 && download_url1 == download_url2
    }

    @Nullable
    override fun getChangePayload(oldPosition: Int, newPosition: Int): Any? {
        return super.getChangePayload(oldPosition, newPosition)
    }
}