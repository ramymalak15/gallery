package com.demo.gallery.ui.main

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.demo.gallery.GalleryApplication
import com.demo.gallery.databinding.MainActivityBinding
import com.demo.gallery.di.component.DaggerActivityComponent
import com.demo.gallery.di.module.ActivityModule
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var mainViewModel: MainViewModel



    private lateinit var binding: MainActivityBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencies()
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupObservers()
    }


    private fun setupObservers() {
        mainViewModel.isConnected.observe(this, {
            binding.offlineModeTv.visibility = if(it == true) View.INVISIBLE else View.VISIBLE
        })
    }

    private fun injectDependencies() {
        DaggerActivityComponent
            .builder()
            .applicationComponent((application as GalleryApplication).applicationComponent)
            .activityModule(ActivityModule(this))
            .build()
            .inject(this)
    }
}