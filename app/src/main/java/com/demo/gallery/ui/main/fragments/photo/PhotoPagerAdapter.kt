package com.demo.gallery.ui.main.fragments.photo

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DiffUtil
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.demo.gallery.data.remote.response.PhotoItem
import com.demo.gallery.ui.main.fragments.photo.details.DetailsFragment
import com.demo.gallery.ui.main.fragments.PhotoDiffUtil
import com.demo.gallery.utils.common.Constants

class PhotoPagerAdapter(fragment: Fragment, val photos: ArrayList<PhotoItem>): FragmentStateAdapter(fragment)  {
    override fun getItemCount(): Int = photos.size

    override fun createFragment(position: Int): Fragment {
        val fragment = DetailsFragment()
        val args = Bundle()
        args.putParcelable(
            Constants.PHOTO_ITEM,
            photos[position]
        )
        fragment.arguments = args
        return fragment
    }

    fun updateList(list: List<PhotoItem>){
        val diffCallback = PhotoDiffUtil(photos, list)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        photos.clear()
        photos.addAll(list)
        diffResult.dispatchUpdatesTo(this)
    }
}