package com.demo.gallery.data.remote

import com.demo.gallery.data.remote.response.PhotoItem
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface NetworkService {

    @GET(EndPoints.LIST)
    fun getPhotos(@Query("page") page: Int, @Query("limit") limit: Int): Single<List<PhotoItem>>
}