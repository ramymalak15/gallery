package com.demo.gallery.data

import com.demo.gallery.data.remote.NetworkService
import javax.inject.Inject

class PhotoRepository @Inject constructor(
    private val networkService: NetworkService
) {

    fun fetchPhotos(page: Int, limit:Int = 5) = networkService.getPhotos(page, limit)
}