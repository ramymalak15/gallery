package com.demo.gallery.data.remote

import androidx.viewbinding.BuildConfig
import com.demo.gallery.utils.network.NetworkHelper
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit

object Networking {
    private const val NETWORK_CALL_TIMEOUT = 10

    fun create(
        baseUrl: String,
        cacheDir: File,
        cacheSize: Long,
        networkHelper: NetworkHelper
    ): NetworkService =
        Retrofit
            .Builder()
            .baseUrl(baseUrl)
            .client(
                OkHttpClient.Builder()
                    .cache(Cache(cacheDir, cacheSize))
                    .addInterceptor(
                        HttpLoggingInterceptor()
                            .apply {
                                level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
                                else HttpLoggingInterceptor.Level.NONE
                            })
                    .addInterceptor { chain ->
                        var request: Request = chain.request()
                        if (!networkHelper.isNetworkConnected()) {
                            val maxStale = 60 * 60 * 24 * 30
                            request = request.newBuilder()
                                .header(
                                    "Cache-Control",
                                    "public, only-if-cached, max-stale=$maxStale"
                                )
                                .removeHeader("Pragma")
                                .build()
                        }
                        return@addInterceptor chain.proceed(request)
                    }
                    .addNetworkInterceptor { chain ->
                        val response = chain.proceed(chain.request())
                        val maxAge = 60

                        return@addNetworkInterceptor response.newBuilder()
                            .header("Cache-Control", "public, max-age=$maxAge")
                            .removeHeader("Pragma")
                            .build()
                    }
                    .readTimeout(NETWORK_CALL_TIMEOUT.toLong(), TimeUnit.SECONDS)
                    .writeTimeout(NETWORK_CALL_TIMEOUT.toLong(), TimeUnit.SECONDS)
                    .build()
            )
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .build()
            .create(NetworkService::class.java)

}